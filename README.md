# Scalable Simple Server

Proof of concept to demonstrate the use of Ansible for configuring a simple Flask
web server inside of a private network replicated to multiple nodes using Vagrant.

Each `worker` node will respond with ip address, system build and 
datetime information when an http request is made at the root path of the host. 
Since this is on a private network, an `observer` node with ssh access can be 
used to access each `worker` node. Each node is configured with 1 core and 512mb 
of ram. IP addresses begin at `10.0.0.10` (observer) and workers increment by one 
from `.10` (`10.0.0.11, .12, .13 ...`)

## Install Requirements 
1. [VirtualBox - Oracle](https://www.virtualbox.org/wiki/Downloads)
2. [Vagrant - HashiCorp](https://www.vagrantup.com/downloads.html)

# Install
To create 2 worker nodes and 1 observer, do the following:
1. Clone this project
2. Run `vagrant up` from the project root:
```
01:33:00 brett@porkchopsandwiches ~/work/experimental/scalable-simple-server $ vagrant up
Bringing machine 'worker-1' up with 'virtualbox' provider...
Bringing machine 'worker-2' up with 'virtualbox' provider...
Bringing machine 'observer' up with 'virtualbox' provider...
==> worker-1: Importing base box 'ubuntu/bionic64'...
==> worker-1: Matching MAC address for NAT networking...
==> worker-1: Checking if box 'ubuntu/bionic64' version '20191205.0.0' is up to date...
==> worker-1: Setting the name of the VM: worker-1
[ ... ]

```

This will create the following VMs:
```
observer: 10.0.0.10
worker-1: 10.0.0.11
worker-2: 10.0.0.12
```

You can also see these running inside of VirtualBox:

![](https://i.imgur.com/5CBHY80.png)


The only SSH accessible VM is `observer`


# Proof of concept
1. Accessing from observer. Run `vagrant ssh observer`
```
01:15:20 brett@porkchopsandwiches ~/work/experimental/scalable-simple-server $ vagrant ssh observer
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-72-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun Dec  8 21:15:31 UTC 2019

  System load:  0.18              Processes:             89
  Usage of /:   13.7% of 9.63GB   Users logged in:       0
  Memory usage: 24%               IP address for enp0s3: 10.0.2.15
  Swap usage:   0%                IP address for enp0s8: 10.0.0.10


0 packages can be updated.
0 updates are security updates.


vagrant@observer:~$
```

2. Show open ports of both workers using `nmap 10.0.0.11-12` from observer
```
vagrant@observer:~$ nmap 10.0.0.11-12

Starting Nmap 7.60 ( https://nmap.org ) at 2019-12-08 21:23 UTC
Nmap scan report for 10.0.0.11
Host is up (0.00036s latency).
Not shown: 998 filtered ports
PORT    STATE  SERVICE
80/tcp  open   http
443/tcp closed https

Nmap scan report for 10.0.0.12
Host is up (0.00054s latency).
Not shown: 998 filtered ports
PORT    STATE  SERVICE
80/tcp  open   http
443/tcp closed https

Nmap done: 2 IP addresses (2 hosts up) scanned in 7.10 seconds
```

3. Get contents from `http://<worker-ip>` using `curl` to show web server is running 
```
vagrant@observer:~$ curl http://10.0.0.11
{"ip_address":"10.0.2.15","platform":"Linux-4.15.0-72-generic-x86_64-with-Ubuntu-18.04-bionic","timestamp":"2019-12-08 21:24:12.809391"}
vagrant@observer:~$
vagrant@observer:~$ curl http://10.0.0.12
{"ip_address":"10.0.2.15","platform":"Linux-4.15.0-72-generic-x86_64-with-Ubuntu-18.04-bionic","timestamp":"2019-12-08 21:24:14.532094"}
vagrant@observer:~$ 
```

# Scaling the project
Scaling is super easy with this project. Simply edit the `Vagrantfile` and update this section:
```
# -----------------------------------------------------------------------------
# Variables for configuring our deployment                                    #
# -----------------------------------------------------------------------------
vm_memory = 512
cpu_count = 1

worker_count = 2       # <---- Scale number of worker nodes

subnet = "10.0.0"
observer_ip = "#{subnet}.10"
# -----------------------------------------------------------------------------

```

# Tearing down
To shut down VMs and delete them, run `vagrant destroy -f` from the host machine
```
01:32:48 brett@porkchopsandwiches ~/work/experimental/scalable-simple-server $ vagrant destroy -f                                                                          
==> observer: Forcing shutdown of VM...                                                                                                                                    
==> observer: Destroying VM and associated drives...                                                                                                                       
==> worker-2: Forcing shutdown of VM...                                                                                                                                    
==> worker-2: Destroying VM and associated drives...                                                                                                                       
==> worker-1: Forcing shutdown of VM...                                                                                                                                    
==> worker-1: Destroying VM and associated drives... 
```


# Final Thoughts
1. When creating this project, I would get the correct IP address when testing locally. Since this is on a private network VM, 
it seems to be pulling the IP address from the `enp0s3` interface which is the same per host. Perhaps I could switch to 
using the `netifaces` python library instead of `socket` for this PoC like below:
```
import netifaces as ni
ni.ifaddresses('eth0')
ip = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
print ip  # should print "192.168.100.37"

# (source https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-nic-in-python)
```
2. Instead of using cURL, I could have also forwarded x11 over ssh to prove that these addresses are accessible via web browser. Apparently this can be done with `config.ssh.forward_x11 = true` (source: https://coderwall.com/p/ozhfva/run-graphical-programs-within-vagrantboxes). Perhaps in a later iteration.
3. My initial plan was to host the webservers within docker containers and create a docker swarm. After experimenting with that a bit, it proved to be far more cumbersome to manage overlay networks and container to container communication than it was worth. The project quickly became focused around container orchestration than VM replication. Vagrant and Ansible seemed to be the best solution for this since I had already begun looking at Ansible for other side projects, so it was a good opportunity to learn more about it.