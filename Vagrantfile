# -*- mode: ruby -*-
# vi: set ft=ruby :

# -----------------------------------------------------------------------------
# Variables for configuring our deployment                                    #
# -----------------------------------------------------------------------------
vm_memory = 512
cpu_count = 1

worker_count = 2

subnet = "10.0.0"
observer_ip = "#{subnet}.10"
# -----------------------------------------------------------------------------


Vagrant.configure("2") do |config| 
  # Create worker-1, worker-2, worker-N ... 
  (1..worker_count).each do |i|
    config.vm.define "worker-#{i}" do |worker|
      worker.vm.box = "ubuntu/bionic64"
      worker.vm.hostname = "worker-#{i}"

      # Increment port from 10.0.0.10+i
      worker.vm.network "private_network", ip: "#{subnet}.#{10 + i}",
        virtualbox__intnet: true
      
      # Configure worker vm
      worker.vm.provider :virtualbox do |vb|
        vb.memory = vm_memory
        vb.cpus = cpu_count
        vb.name = "worker-#{i}"
      end

      # Run our worker ansible playbook.
      worker.vm.provision :ansible_local do |ansible|
        ansible.playbook = "playbooks/worker-playbook.yaml"
      end
    end
  end

  # Create an observer node on the same private network
  config.vm.define "observer", primary: true do |observer|
    observer.vm.box = "ubuntu/bionic64"
    observer.vm.hostname = "observer"

    # Use 10.0.0.10. 
    observer.vm.network "private_network", ip: observer_ip,
      virtualbox__intnet: true

    # Configure observer vm
    observer.vm.provider :virtualbox do |vb|
      vb.memory = vm_memory
      vb.cpus = cpu_count
      vb.name = "observer"
    end

    # Run our observer ansible playbook.
    observer.vm.provision :ansible_local do |ansible|
      ansible.playbook = "playbooks/observer-playbook.yaml"
    end
  end
end