from app import app

# For gunicorn to execute as a WSGI HTTP Server
if __name__ == "__main__":
  app.run()
