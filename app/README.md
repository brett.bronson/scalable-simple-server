# App
All files needed to run the webserver.

# Testing locally
```
sudo apt-get install python3 python3-pip 
pip3 install virtualenv
virtualenv simple-webserver
source venv/bin/activate
pip3 install -r requirements.txt
./app.py
```
Then visit http://localhost:5000