#! /usr/bin/env python3

__version__ = '0.0.1'
__author__ = 'Brett Bronson'

import contextlib
import platform
import socket

from datetime import datetime
from flask import Flask, jsonify


app = Flask(__name__)


@app.route('/')
def index():
    '''Simple python web service which outputs a RESTful friendly JSON payload
    when accessing the root of the service.  

    Example response:
        {
            "ip_address": "192.168.2.46",
            "platform": "Linux-4.15.0-70-generic-x86_64-with-LinuxMint-19.1-tessa",
            "timestamp": "2019-12-04 06:51:27.392773"
        }

    :return: JSON payload
    '''
    @contextlib.contextmanager
    def _get_ip():
        '''Prevents returning a value like 127.0.0.1 or 127.0.1.1 when the 
        host is defined in /etc/hosts (can occur on debian systems).

        :yield: String representation of the host's IP address
        '''
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 1))
        yield s.getsockname()[0]
        s.close()

    with _get_ip() as _ip:
        ip = _ip

    return jsonify({
        'platform': platform.platform(),
        'timestamp': str(datetime.utcnow()),
        'ip_address': ip
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
